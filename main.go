package main

import (
	"fmt"
	"gitlab.com/eniehack/monkeyc/repl"
	"os"
	"os/user"
)

func main() {
	user, err := user.Current()
	if err != nil {
		panic(err)
	}
	fmt.Printf("Hello, %s! This is the Monkey Programming Language's REPL.\n", user.Username)
	repl.Start(os.Stdin, os.Stdout)
}
